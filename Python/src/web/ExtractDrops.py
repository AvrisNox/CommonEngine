import urllib.request
from lxml import html

def run(*args, **kwargs):
    main_url = f"https://forums.warframe.com/forum/113-livestreams-contests/"
    main_targ = f"//span/a[contains(@href, 'community-stream-schedule')][1]/@href"
    tree = html.fromstring(urllib.request.urlopen(main_url).read())
    drop_url = tree.xpath(main_targ)[0]
    # This URL is an example of a "Week" drop
    # drop_url = "https://forums.warframe.com/topic/1250004-community-stream-schedule-february-22-26/"      # Works
    # This URL is an example of a "Grab bag" drop
    # drop_url = "https://forums.warframe.com/topic/1251033-community-stream-schedule-march-1-5/"           # Works
    # drop_url = "https://forums.warframe.com/topic/1251930-community-stream-schedule-march-8-12/"          # Works
    # This URL is an example of a "Pot" drop
    # drop_url = "https://forums.warframe.com/topic/1252773-community-stream-schedule-march-15-19/"         # Works

    drop_targ = f"//article[1]//div[contains(@class, 'ipsType_normal')]//ul"
    tree = html.fromstring(urllib.request.urlopen(drop_url).read())
    content = tree.xpath(drop_targ)[0]

    options = []

    # Week: Each day has something specific to offer
    week_cleaned = []
    not_week_flag = False
    for item in content:
        htmlstr = str(html.tostring(item)).replace("\\n", "").replace("\\t", "").replace(":", "")
        tmp = htmlstr.split(">")
        tmp = tmp[2].split("<")[0].strip()
        if len(tmp) > 0:
            if 'monday' in tmp.lower():
                not_week_flag = True
            week_cleaned.append(tmp)
            pass
    # Grab bag: There are a few items that can be acquired throughout the week
    grabbag_cleaned = []
    for child in content:
        for baby in child:
            for item in baby:
                htmlstr = str(html.tostring(item)).replace("\\n", "").replace("\\t", "").replace(":", "")
                tmp = htmlstr.split(">")
                tmp = tmp[1].strip()
                tmp = tmp.split("<")[0].strip()
                if len(tmp) > 0:
                    grabbag_cleaned.append(tmp)
                    pass
    # Pot: There are a few items that can be acquired throughout the week
    pot_cleaned = []
    for child in content:
        for item in child:
            htmlstr = str(html.tostring(item)).replace("\\n", "").replace("\\t", "").replace(":", "")
            tmp = htmlstr.split(">")
            tmp = tmp[len(tmp) - 1].strip()
            tmp = tmp.split("<")[0].strip()
            if len(tmp) > 0:
                pot_cleaned.append(tmp)
                pass

    # Put the grab bag in first
    if len(grabbag_cleaned) > 0:
        options.append(grabbag_cleaned)
    # If week is 4 or 5 element(s), put that first
    len_week = len(week_cleaned)
    if len_week == 4 or len_week == 5:
        options.insert(0, week_cleaned)
    else:
        options.append(week_cleaned)
    # If pot is 1 element, put that first
    len_pot = len(pot_cleaned)
    if len_pot == 1 or not_week_flag:
        options.insert(0, pot_cleaned)
    elif len_pot > 0:
        options.append(pot_cleaned)

    print(options)
    return options
