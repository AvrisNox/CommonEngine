import gzip


def run(*args, **kwargs):
    with gzip.open(args[1], 'rb') as fin:
        content = fin.read()
    print(content)
