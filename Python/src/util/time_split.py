def run(*args, **kwargs):
    strs = args[1].split(" ")
    mp = {}

    for i in range(0, len(strs)):
        inpt = strs[i][2:len(strs[i]) - 2]


        key = int(inpt.split("(")[1].split(")")[0])
        val = int(inpt.split(":")[1])

        if mp[key] is None:
            mp[key] = []
        mp[key].append(val)
