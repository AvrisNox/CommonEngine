def run(*args, **kwargs):
    import gzip

    with gzip.open(f'{args[1]}/train-images-idx3-ubyte.gz', 'rb') as fin:
        raw_digit_images = fin.read()
    with gzip.open(f'{args[1]}/train-labels-idx1-ubyte.gz', 'rb') as fin:
        raw_digit_labels = fin.read()

    # First 16 bytes of images and 8 bytes of labels are meta data
    digit_images = []
    for z in range(0, 60000):
        current_image = []
        for i in range(0, 784):
            current_image.append(raw_digit_images[16 + z*784 + i])
        digit_images.append(current_image)
    print(len(digit_images))

    digit_labels = []
    for z in range(0, 60000):
        digit_labels.append(raw_digit_labels[8 + z])
    print(len(digit_labels))

    # -- STOP HERE
    from PIL import Image
    img = Image.new('RGB', (28, 28), 'red')
    pixels = img.load()
    for y in range(0, 28):
        for x in range(0, 28):
            pixels[x, y] = digit_images[27][y*28 + x]
    img.show()

    print(f"Output image label is: {digit_labels[27]}")
