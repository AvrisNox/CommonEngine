import os, zipfile

def run(*args, **kwargs):
    if len(args) == 1 and "path" not in kwargs.keys():
        print("Need path")
        return

    if len(args) > 1:
        path = args[1]
    else:
        path = kwargs["path"]
    direc = os.fsencode(path)

    files = []
    for file in os.listdir(direc):
        fn = os.fsdecode(file)
        if fn.endswith(".zip"):
            files.append(file)

    print(f"Found {len(files)} files to unzip. Unzipping...")
    # TODO: Add a progress report
    for file in files:
        fn = os.fsdecode(file)
        with zipfile.ZipFile(os.path.join(path, fn)) as zip_ref:
            zip_ref.extractall(path)

    pass
